<?php


/**
 * @file
 * Wysiwyg API integration.
 */

/**
 * Implementation of hook_wysiwyg_plugin().
 */
function wysiwyg_quote_quote_plugin() {
  $plugins['quote'] = array(
    'title' => t('Quote'),
    'vendor url' => 'http://drupal.org/sandbox/zolexiy/1166988',
    'icon file' => 'q.png',
    'icon title' => t('Insert quote tag'),
    'settings' => array(),
  );
  return $plugins;
}
